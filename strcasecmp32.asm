global _stricmp
SECTION .text  align=16
_stricmp:
        mov     ecx, [esp+4]           ; string_a
        mov     edx, [esp+8]           ; string_b
        sub     edx, ecx
        
next_byte:    mov     al,  [ecx]
        cmp     al,  [ecx+edx]
        jne     check_case
        inc     ecx
        test    al, al
        jnz     next_byte                    
        
        ; terminating found. Strings equal
        xor     eax, eax
        ret        
        
check_case:    ; bytes are different. check case
        xor     al, 20H                ; toggle case
        cmp     al, [ecx+edx]
        jne     bytes_diff
        or      al, 20H                ; upper case
        sub     al, 'a'
        cmp     al, 'z'-'a'
        ja      bytes_diff                    
        inc     ecx
        jmp     next_byte               
        
bytes_diff:    ; bytes are different, even after changing case
        movzx   eax, byte [ecx]       
        sub     eax, 'A'
        cmp     eax, 'Z' - 'A'
        ja      bytes_diff2
        add     eax, 20H
bytes_diff2:    
        movzx   edx, byte [ecx+edx]
        sub     edx, 'A'
        cmp     edx, 'Z' - 'A'
        ja      fin
        add     edx, 20H
fin:    sub     eax, edx                 ; subtract to get result
        ret
